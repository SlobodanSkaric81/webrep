<?php

namespace App\Core\Fingerprint;

class BasicFingerprintProviderFactory{
    public function getInstance(string $arraySource): BasicFingerPrintProvider{
        switch ($arraySource){
            case "SERVER":
                return new BasicFingerPrintProvider($_SERVER);
        }

        return new BasicFingerPrintProvider($_SERVER);
    }
}