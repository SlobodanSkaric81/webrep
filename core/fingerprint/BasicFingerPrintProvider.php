<?php

namespace App\Core\Fingerprint;

class BasicFingerPrintProvider implements FingerprintProvider{
    private $data;

    public function __construct(array $data){
        $this->data = $data;
    }
    public function provideFingerprint(): string{
        $userAgenet = filter_var($this->data["HTTP_USER_AGENT"] ?? "", FILTER_SANITIZE_STRING);
        $ipAddres = filter_var($this->data["REMOTE_ADDR"] ?? "", FILTER_SANITIZE_STRING);

        $string = $userAgenet . "|" . $ipAddres;
        $hash1 = hash("sha512", $string);
        return hash("sha512", $hash1);
    }
}