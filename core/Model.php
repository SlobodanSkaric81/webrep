<?php

    namespace App\Core;

use Exception;

abstract class Model{
        private $dbc;

        final public function __construct(DatabaseConnection &$dbc){
            $this->dbc = $dbc;
        }

         public function getConnection(){
            return $this->dbc->getConnection();
        }

        protected function getFields(){
            return [];
        }

        final protected function getTableName(){
            $fullName = static::class;
            $matches = [];

            preg_match("|^.*\\\((?:[A-Z][a-z]+)+)Models$|", $fullName, $matches);
            $className = $matches[1] ?? "";
            $underscoreClassName = preg_replace("|[A-Z]|", "_$0", $className);
            $lowerCaseClassName = strtolower($underscoreClassName);

            return substr($lowerCaseClassName, 1);
          }

        final public function getById($id){
            $tableName = $this->getTableName();
            $sql = "SELECT * FROM " . $tableName . " WHERE " . $tableName."_id = ?";
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute([$id]);
            $item = NULL;

            if($res){
                $item = $prep->fetch(\PDO::FETCH_OBJ);
            }

            return $item;
        }

        final public function getAll(){
            $tableName = $this->getTableName();
            $sql = "SELECT * FROM " .  $tableName;
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute();
            $items = [];

            if($res){
                $items = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return $items;
        }

         private function isFieldValueValid($fieldName, $fieladValue){
           $field = $this->getFields();
           $supportedFieleName = array_keys($field);

           if(!in_array($fieldName,  $supportedFieleName)){
               return false;
           }

           return $field[$fieldName]->isValid($fieladValue);
       }

        final public function getFillName($fileName, $value){
            if(!$this->isFieldValueValid($fileName,$value)){
                new Exception("Invalid file name: " . $fileName);
            }

            $tableName = $this->getTableName();
            $sql = "SELECT * FROM " . $tableName . " WHERE " . $fileName . " = ?";
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute([$value]);
            $item = NULL;

            if($res){
                $item = $prep->fetch(\PDO::FETCH_OBJ);
            }

            return $item;
        }

        final public function getAllFillName($fileName, $value){
            if(!$this->isFieldValueValid($fileName,$value)){
                new \Exception("Invalid file name: " . $fileName);
            }

            $tableName = $this->getTableName();
            $sql = "SELECT * FROM " . $tableName . " WHERE " . $fileName . " = ?";
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute([$value]);
            $items = [];

            if($res){
                $items = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return $items;
        }
        private function checkFieldList($data){
            $fields = $this->getFields();
            
            $supportedFieldNames = array_keys($fields);
            $requestedFildNames = array_keys($data);

            foreach( $requestedFildNames as $requestedFildName){
                if(!in_array($requestedFildName, $supportedFieldNames)){
                    throw new \Exception("Field name " . $requestedFildName . " is not supported");
                }

                if(!$fields[$requestedFildName]->isEditable()){
                    throw new \Exception("Field name " . $requestedFildName . " is not editable");
                }

                if(!$fields[$requestedFildName]->isValid($data[$requestedFildName])){
                    throw new \Exception("Value field name " . $requestedFildName . " is not supported");
                }
            }
        }

        final public function add($data){
            $this->checkFieldList($data);
            
            $tableName = $this-> getTableName();
            $sqlFieldNames = implode(", ", array_keys($data));
            $questionMarks = str_repeat("?,", count($data));
            $questionMark = substr($questionMarks,0,-1);

            $sql = "INSERT INTO {$tableName} ({$sqlFieldNames}) VALUES ({$questionMark})";
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute(array_values($data));

            if(!$res){
               return false;
            }

            return $this->dbc->getConnection()->lastInsertId();
        }

        final public function edit($id, $data){
            $this->checkFieldList($data);

            $tableName = $this->getTableName();

            $editList = [];
            $valuesList = [];

            foreach($data as $fieldName => $values){
                $editList [] = "{$fieldName} = ?";
                $valuesList[] = $values;
            }

            $editString = implode(", ", $editList);
            
            $valuesList[] = $id;

            $sql = "UPDATE {$tableName} SET {$editString} WHERE {$tableName}_id=?";
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute($valuesList);
            
            return $res;
            
        }

        final public function deleteById($id){
            $tableName = $this->getTableName();
            $sql = "DELETE FROM " . $tableName . " WHERE " . $tableName."_id = ?";
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute([$id]);
           
            return $res;
        }
    }