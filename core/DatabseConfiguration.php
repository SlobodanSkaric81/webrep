<?php
    namespace App\Core;

    class DatabseConfiguration{
        private $host;
        private $dbname;
        private $username;
        private $password;

        public function __construct($host, $dbname, $username, $password) {
            $this->host = $host;
            $this->dbname = $dbname;
            $this->username = $username;
            $this->password = $password;
        }

        public function getSource(){
            return "mysql:host={$this->host};dbname={$this->dbname};charset=utf8";
        }

        public function getUserName(){
            return $this->username;
        }

        public function getPassword(){
            return $this->password;
        }
    }