<?php
 namespace App\Core;

 final class Field{

        private $validator;
        private $editable;

        public function __construct(Validator $validator, $edit) {
            $this->validator = $validator;
            $this->editable = $edit;
        }

        public function isValid($value){
           return $this->validator->isValid($value);
        }

        public function isEditable(){
            return $this->editable;
        }

        #is old method vidation

        /*private $pattern;  
        private $editable;

        public function __construct($pattern, $editable) {
            $this->pattern = $pattern;
            $this->editable = $editable;
        }

        public function isValid($value){
            return preg_match($this->pattern, $value);
        }

        public function isEditable(){
            return $this->editable;
        }

        public static function editableIpAddress(){
            return new Field("@^([0-9]{1,3}(\.[0-9]{1,3}){3})|(::1)$@", true);
        }

        public static function readonlyIpAddress(){
            return new Field("@^([0-9]{1,3}(\.[0-9]{1,3}){3})|(::1)$@", false);
        }

        public static function editableString($string){
            return new Field("|^.{0," . $string . "}$|", true);
        }

        public static function readonlyString($string){
            return new Field("|^.{0," . $string . "}$|", false);
        }

        public static function editableInitiger($intiger){
            return new Field("|^[1-9][0-9]{0," . ($intiger - 1). "}$|", true);
        }

        public static function readonlyInitiger($intiger){
            return new Field("^[1-9][0-9]{0," . ($intiger - 1). "}$", false);
        }

        public static function editableFixedDecimal($intiger, $decimal){
            return new Field("|^[1-9][0-9]{0," . ($intiger - 1). "}\.[0-9]{" . $decimal . "}$|", true);
        }

        public static function readonlyFixedDecimal($intiger, $decimal){
            return new Field("|^[1-9][0-9]{0," . ($intiger - 1). "}\.[0-9]{" . $decimal . "}$|", false);
        }

        public static function editableMaxdDecimal($intiger, $decimal){
            return new Field("|^\-?[1-9][0-9]{0," . ($intiger - 1). "}\.[0-9]{0," . $decimal . "}$|", true);
        }

        public static function readonlyMaxdDecimal($intiger, $decimal){
            return new Field("^|\-?[1-9][0-9]{0," . ($intiger - 1). "}\.[0-9]{0," . $decimal . "}$|", false);
        }

        public static function editablelyDateTime(){
            return new Field("|^[1-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$|", true);
        }

        public static function redonlyDateTime(){
            return new Field("|^[1-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$|", false);
        }

        public static function editablelyDate(){
            return new Field("|^[1-9]{4}\-[0-9]{2}\-[0-9]{2}$|", true);
        }

        public static function redonlyDate(){
            return new Field("|^[1-9]{4}\-[0-9]{2}\-[0-9]{2}$|", false);
        }

        public static function editablelyTime(){
            return new Field("|^[0-9]{2}:[0-9]{2}:[0-9]{2}$|", true);
        }

        public static function redonlyTime(){
            return new Field("|^[0-9]{2}:[0-9]{2}:[0-9]{2}$|", false);
        }

        public static function editablelyBit(){
            return new Field("|^[01]$|", true);
        }

        public static function redonlyBit(){
            return new Field("|^[01]$|", false);
        }*/
 }