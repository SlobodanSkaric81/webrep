<?php
    
    namespace App\Core;

    class DatabaseConnection{
        private $connection;
        private $configuration;

        public function __construct(DatabseConfiguration $dbConf) {
            $this->configuration = $dbConf;
        }

        public function getConnection(){
            if($this->connection == NULL){
                $this->connection = new \PDO($this->configuration->getSource(), $this->configuration->getUserName(),$this->configuration->getPassword());
            }

            return $this->connection;
        }
    }