<?php

    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\BitValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\NumberValidator;
    use App\Validators\StirngValidator;
    class OfferModels extends Model{

        protected function getFields(){
            return [
                "offer_id"    => new Field((new NumberValidator())->setIntegerlength(11), false),
                "created_at"  => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),

                "auction_id"  => new Field((new NumberValidator())->setIntegerlength(11), true),
                "user_id"     => new Field((new NumberValidator())->setIntegerlength(11), true),
                "price"       => new Field((new NumberValidator())->setIntegerlength(11), true)
            ];
        }
        

        public function getAllByAuctionId($auctionId){
            return $this->getAllFillName("auction_id", $auctionId);
            /*$sql = "SELECT * FROM offer WHERE auction_id = ?";
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute([$auctionId]);
            $offers = [];

            if($res){
                $offers = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return $offers;*/

        }

        public function getLastByAuctionId(int $auctionId){
            $sql = "SELECT * FROM offer WHERE auction_id = ?  ORDER BY created_at DESC LIMIT 1";
            $prep = $this->getConnection()->prepare($sql);
            $execute = $prep->execute([$auctionId]);


            if(!$execute){
                return null;
            }

            return $prep->fetch(\PDO::FETCH_OBJ);

        }

        public function getlastOfferPrice($auction){
            $result = $this->getLastByAuctionId($auction->auction_id);

            if(!$result ){
                return $auction->starting_price;
            }

            return  $result->price;


        }

    }