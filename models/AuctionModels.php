<?php
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\BitValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\NumberValidator;
    use App\Validators\StirngValidator;

class AuctionModels extends Model{
        protected function getFields(){
                return [
                    "auction_id"      => new Field((new NumberValidator())->setIntegerlength(11), false),
                    "created_at"      => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),

                    "title"           => new Field((new StirngValidator)->setMaxlength(255),true),
                    "description"     => new Field((new StirngValidator)->setMaxlength(300),true),
                    "starting_price"  => new Field((new NumberValidator())->setDecimal()->setUnsigned()->setIntegerlength(7)->setMaxDecimalDigits(2),true),
                    "start_at"        => new Field((new DateTimeValidator())->allowDate(),true),
                    "end_at"          => new Field((new DateTimeValidator())->allowDate(),true),
                  //  "is_active"       => new Field(new BitValidator(),true),
                    "category_id"     => new Field((new NumberValidator())->setIntegerlength(3),true),
                    //"img"             => new Field((new StirngValidator)->setMaxlength(255),true),
                    "user_id"         => new Field((new NumberValidator())->setIntegerlength(11),true)
                ];

                 /*old method vlidation
                  return [
                    "auction_id"      => Field::readonlyInitiger(11),
                    "created_at"      => Field::redonlyDateTime(),

                    "title"           => Field::editableString(255),
                    "description"     => Field::editableString(300),
                    "starting_price"  => Field::editableInitiger(255),
                    "start_at"        => Field::editablelyDateTime(),
                    "end_at"          => Field::editablelyDateTime(),
                    "is_active"       => Field::editablelyBit(),
                    "category_id"     => Field::editableInitiger(3)
                ]; */
        }
       

        public function getAllByCategoryId(int $categoryId): array{
            return $this->getAllFillName("category_id", $categoryId);
            /*$sql = "SELECT * FROM auction WHERE category_id = ? ORDER BY created_at ASC";
            $prep =  $this->getConnection()->prepare($sql);
            $res = $prep->execute([$categoryId]);
            $auctions = [];

            if($res){
                $auctions = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return $auctions;*/
        }

        public function getAllByUserId(int $userId): array {
            return $this->getAllFillName("user_id", $userId);
        }


    public function getAllBySearc(string $keywors){
            $sql = "SELECT * FROM auction WHERE title LIKE ? OR description LIKE ?;";
            if($keywors == ""){
                return "";
            }
            $prep = $this->getConnection()->prepare($sql);
            $keywors = "%" . $keywors . "%";

            if(!$prep){
                return [];
            }

            $res = $prep->execute([$keywors,$keywors]);
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }

    }