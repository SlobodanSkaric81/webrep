<?php

    namespace App\Models;

    
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\BitValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\NumberValidator;
    use App\Validators\StirngValidator;

    class CategoryModels extends Model{

        protected function getFields(){
            return [
                "category_id"        => new Field((new NumberValidator())->setIntegerlength(11), false),

                "users"              => new Field((new StirngValidator)->setMaxlength(255),true),
                "skills"             => new Field((new StirngValidator)->setMaxlength(255),true),
                "administration"     => new Field((new StirngValidator)->setMaxlength(255),true)
            ];
        }
       

      
    }