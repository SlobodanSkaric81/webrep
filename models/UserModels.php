<?php

    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\BitValidator;
use App\Validators\DateTimeValidator;
use App\Validators\NumberValidator;
use App\Validators\StirngValidator;

    class UserModels extends Model{

        protected function getFields(){
            return [
                "user_id"       => new Field((new NumberValidator())->setIntegerlength(11), false),
                
                "name"          => new Field((new StirngValidator())->setMaxlength(50),true),
                "lastname"      => new Field((new StirngValidator())->setMaxlength(255),true),
                "username"      =>  new Field((new StirngValidator())->setMaxlength(255),true),
                "email"         => new Field((new StirngValidator())->setMaxlength(50),true),
                "password_hash"      => new Field((new StirngValidator())->setMaxlength(128),true),
                "is_activ"         => new Field((new NumberValidator())->setInteger()->setIntegerlength(1),true)
            ];
        }
       
        public function getAllByUsername($username){
            return $this->getAllFillName("name", $username);
            /*$sql = "SELECT * FROM user WHERE name = ?";
            $prep = $this->dbc->getConnection()->prepare($sql);
            $res = $prep->execute([$username]);
            $user = [];

            if($res){
                $user = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return $user;*/
        }

    }