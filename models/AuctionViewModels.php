<?php

namespace App\Models;
use App\Core\Model;
use App\Core\Field;
use App\Validators\BitValidator;
use App\Validators\DateTimeValidator;
use App\Validators\NumberValidator;
use App\Validators\StirngValidator;

class AuctionViewModels extends Model{
    protected function getFields(){
        return [
            /*"ip_address" => new Field("@^([0-9]{1,3}(\.[0-9]{1,3}){3})|(::1)$@", true),
            "user_agent" => new Field("|^.+$|", true),
            "auction_id" => new Field("|^[1-9][0-9]{0,10}$|", true)*/
            "auction_view_id" => new Field((new NumberValidator())->setIntegerlength(11), false),
            "created_at"      => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
            
            "ip_address"      => new Field((new StirngValidator)->setMaxlength(255),true),
            "user_agent"      => new Field((new StirngValidator)->setMaxlength(255),true),
            "auction_id"      => new Field((new StirngValidator)->setMaxlength(255),true)
        ];
    }


    public function getAllByAuctionId($auctionId){
        return $this->getAllFillName("auction_id", $auctionId);
    }

    public function getAllByIpAddres($ipAddres){
        return $this->getAllFillName("ip_address", $ipAddres);
    }
}