<?php
    namespace App\Controllers;

    use App\Models\CategoryModels;
    use App\Core\Controller;
    use App\Models\AuctionViewModels;
    use App\Models\AuctionModels;

    class MainController extends Controller{
        
        public function home(){
            $categoryModel = new CategoryModels($this->getConnection());
            $categorys = $categoryModel->getAll();

           $this->set("categorys", $categorys);


           /*$oldValue = $this->getSession()->get("brojac", 0);
           $newValue = $oldValue + 1;
           $this->getSession()->put("brojac", $newValue);*/
           
           //$this->set("podatak", $newValue);

           #$auctionViewModel = new AuctionModels($this->getConnection());
         

            /*$auctionViewModel->add([
                    "title"           => "ovo je neki title za probu",
                    "description"     => "ovo je neki description za probu",
                    "starting_price"  => "100.18",
                    "start_at"        => "2021-03-04 10:00:01",
                    "end_at"          => "2021-03-05 10:00:01",
                    "is_active"       => 1,
                    "category_id"     =>  3
            ]);*/
        }

        public function getRegister(){

        }

        public function postRegister(){
            $email = filter_input(INPUT_POST, "reg_email", FILTER_SANITIZE_EMAIL);
            $forname = filter_input(INPUT_POST, "reg_forname", FILTER_SANITIZE_STRING);
            $surnaem = filter_input(INPUT_POST,"reg_surname", FILTER_SANITIZE_STRING);
            $username = filter_input(INPUT_POST, "reg_username", FILTER_SANITIZE_STRING);
            $password = filter_input(INPUT_POST, "reg_password", FILTER_SANITIZE_STRING);
            $password_again = filter_input(INPUT_POST, "reg_password_again", FILTER_SANITIZE_STRING);

           /* print_r([
                $emai,
                $forname,
                $surnaem,
                $password,
                $password_again
            ]);*/

           if($password !== $password_again){
               $this->set("message", "Niste uneli uneli dav puta istu lozinku. To je neophodno da biste se rigistrovali...");
               return;
           }

           $stringValidator = (new \App\Validators\StirngValidator())->setMinLength(7)->setMaxlength(255);

           if(!$stringValidator->isValid($password)){
               $this->set("message", "Lozinka mora biti duza od 7 karaktera i manja od 255");
               return;
           }

           $userModel = new \App\Models\UserModels($this->getConnection());

           $userEmail = $userModel->getFillName("email", $email);

           if($userEmail){
               $this->set("message", "Korisnik sa ovom email adrsom vec postoji...");
               return;
           }

            $userName = $userModel->getFillName("username", $username);

            if($userName){
                $this->set("message", "Korisnik sa ovom username vec postoji...");
                return;
            }

            $passwordHash = password_hash($password, PASSWORD_DEFAULT);

            $userId = $userModel->add([
                "name"          =>$forname,
                "lastname"      =>$surnaem,
                "username"      =>$username,
                "email"         =>$email,
                "password_hash" =>$passwordHash
            ]);

            if(!$userId){
                $this->set("message", "Neuspesno registrrovanje naloga...");
                return;
            }


            $this->set("message", "Uspesno ste se registrovsli");

        }

        public function getLogin(){

        }

        public function postLogin(){
            $username = filter_input(INPUT_POST, "login_username", FILTER_SANITIZE_STRING);
            $password = filter_input(INPUT_POST, "login_password", FILTER_SANITIZE_STRING);

            $stringValidator = (new \App\Validators\StirngValidator())->setMinLength(7)->setMaxlength(255);

            if(!$stringValidator->isValid($password)){
                $this->set("message", "Lozinka nije ispravnoh formata");
                return;
            }

            $userModel = new \App\Models\UserModels($this->getConnection());

            $user = $userModel->getFillName("username", $username);

            if(!$user){
                $this->set("message", "Korisnik sa ovom username ne postoji...");
                return;
            }

            $passwordHash = $user->password_hash;

            if(!password_verify($password, $passwordHash)){
                sleep(1);
                $this->set("message", "Password nije isparavan...");
                return;
            }

            $this->getSession()->put("user_id", $user->user_id);
            $this->getSession()->save();
            $this->set("message", "Uspesno ste logovani");

            $this->redirect("/slo/user/profile");

        }

        public function getLogout(){
            $this->getSession()->remove("user_id");
            $this->getSession()->save();

            $this->redirect("/slo/user/login");
        }
    }