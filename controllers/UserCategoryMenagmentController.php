<?php

namespace App\Controllers;


class UserCategoryMenagmentController extends \App\Core\Role\UserRoleController {
    public function categorys(){
        $categoryModel = new \App\Models\CategoryModels($this->getConnection());
        $categorys = $categoryModel->getAll();
        $this->set("categorys", $categorys);
    }

    public function getEdit($categoryId){
        $categoryModel = new \App\Models\CategoryModels($this->getConnection());
        $category = $categoryModel->getById($categoryId);

        if(!$categoryId){
            $this->redirect("/slo/user/categories");
        }

        $this->set("category", $category);

        return $categoryModel;
    }

    public function postEdit($categoryId){
        $categoryModel = $this->getEdit($categoryId);

        $name = filter_input(INPUT_POST, "skills", FILTER_SANITIZE_STRING);

        $categoryModel->edit($categoryId,[
            "skills" => $name
        ]);

        $this->redirect("/slo/user/categories");
    }

    public function getAdd(){

    }

    public function postAdd(){
        $categoryModel = new \App\Models\CategoryModels($this->getConnection());

        $name = filter_input(INPUT_POST, "skills", FILTER_SANITIZE_STRING);

        $categoryModel->add([
            "skills" => $name
        ]);

        $this->set("message", "Dodali ste kategoriju " . $name);

        //$this->redirect("/slo/user/categories");
    }

}