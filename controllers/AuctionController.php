<?php
       namespace App\Controllers;


    use App\Core\Controller;

    class AuctionController extends Controller{
        
        public function show($id){
            $auctionModel = new \App\Models\AuctionModels($this->getConnection());
            $auction = $auctionModel->getById($id);

           /*if(!$auction){
                header("Location: /slo");
                exit;
            }*/
            $this->set("auction", $auction);


            $offerModel = new \App\Models\OfferModels($this->getConnection());
            $lastOfferPrice = $offerModel->getlastOfferPrice($auction);
            
            if(!$lastOfferPrice){
                $lastOfferPrice = $auction->starting_price;
            }

            $this->set("lastOfferPrice", $lastOfferPrice);

            $auctionView = new \App\Models\AuctionViewModels($this->getConnection());
            $ipAddress = filter_input(INPUT_SERVER, "REMOTE_ADDR");
            $userAgent = filter_input(INPUT_SERVER, "HTTP_USER_AGENT");

            $auctionView->add(
                [
                    "ip_address" => $ipAddress,
                    "user_agent" => $userAgent,
                    "auction_id" => $id
                ]
            );
        }

        


        private function normaliseKeywordy(string $keywords){
            $keyw = trim($keywords);
            $keyw = preg_replace("/ +/", " ", $keyw);

            if(preg_match("|^[A-Za-z0-9][A-Za-z0-9]+$|",$keyw)){
                 return $keyw;
            }
            return "";
        }
        public function postSearch(){
            $auctionModel = new \App\Models\AuctionModels($this->getConnection());

            $q = filter_input(INPUT_POST, "q", FILTER_SANITIZE_STRING);
            $keywords = $this->normaliseKeywordy($q);
            if($keywords == ""){
                $this->redirect("/slo/");
            }
            $auction = $auctionModel->getAllBySearc($keywords);




            $this->set("auctions", $auction);
        }
    }