<?php

    namespace App\Controllers;

    use App\Core\Controller;

    class CategoryController extends Controller{

        public function show($id){
            $categoriesModel = new \App\Models\CategoryModels($this->getConnection());
            $category = $categoriesModel->getById($id);

            if(!$category || !isset($category)){
                header("Location:/slo");
                exit;
            }
            $this->set("categoryId", $category);

            $auctionModel = new \App\Models\AuctionModels($this->getConnection());
            $auctionInCategory = $auctionModel->getAllByCategoryId($id);


            $offerModel = new \App\Models\OfferModels($this->getConnection());

            $auctionInCategory = array_map(function($auction) use ($offerModel){
                $auction->last_offer_price = $offerModel->getlastOfferPrice($auction);

                return $auction;
            },$auctionInCategory);

            if(!$auctionInCategory){
                header("Location: /slo");
                exit;
            }

            $this->set("auctions", $auctionInCategory);


        }
    }