<?php
/**
 * Created by PhpStorm.
 * User: sslob
 * Date: 20/06/2021
 * Time: 22:28
 */

namespace App\controllers;


class UserAuctionMenagmentController extends  \App\Core\Role\UserRoleController {
        public function auctions(){
            $userId = $this->getSession()->get("user_id");

            $auctionModel = new \App\Models\AuctionModels($this->getConnection());
            $auction = $auctionModel->getAllByUserId($userId);

            $this->set("auctions", $auction);

        }

        public function getAdd(){
            $categoryModel = new \App\Models\CategoryModels($this->getConnection());
            $categorys = $categoryModel->getAll();

            $this->set("categorys", $categorys);
        }

        public function postAdd(){
            $auctionModel = new \App\Models\AuctionModels($this->getConnection());

            $title              =         filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING);
            $description        =         filter_input(INPUT_POST, "description", FILTER_SANITIZE_STRING);
            $startingprice      =         sprintf("%.2f",filter_input(INPUT_POST, "startingprice", FILTER_SANITIZE_STRING));
            $startat            =         filter_input(INPUT_POST, "startat", FILTER_SANITIZE_STRING);
            $endsat             =         filter_input(INPUT_POST, "endsat", FILTER_SANITIZE_STRING);
            $categoryid         =         filter_input(INPUT_POST, "categoryid", FILTER_SANITIZE_NUMBER_INT);
            $userId             =         $this->getSession()->get("user_id");


            $addData = [
                "title"                 => $title,
                "description"           => $description,
                "starting_price"        => $startingprice,
                "start_at"              => $startat,
                "end_at"                => $endsat,
                "category_id"           => $categoryid,
                "user_id"               => $userId
            ];



            $auctionId = $auctionModel->add($addData);

            if(!$auctionId){
                $this->set("message", "Nije poslata aukcija");
                return;
            }

            if(isset($_FILES["image"]) && $_FILES["image"]["error"] == 0){
                $uploadStatus = $this->doImageUpload("image", $auctionId.".jpg");

                if(!$uploadStatus){
                    $this->set("message", "Aukcija je dodata ali nie dodata njena slika...");
                    return;
                }
            }


            $this->redirect("/slo/user/auction/");
        }

        public function getEdit($auctionId){
            $auctionModel = new \App\Models\AuctionModels($this->getConnection());
            $auction = $auctionModel->getById($auctionId);

            if(!$auction){
                $this->redirect("/slo/user/auction");
            }

            if($auction->user_id != $this->getSession()->get("user_id")){
                $this->redirect("/slo/user/auction");
            }

            $offerModel = new \App\Models\OfferModels($this->getConnection());
            $offer = $offerModel->getAllByAuctionId($auctionId);

            if(count($offer) > 0){
                $this->redirect("/slo/user/auction");
                return;
            }

            $this->set("auction", $auction);

            $categoryModel = new \App\Models\CategoryModels($this->getConnection());
            $category = $categoryModel->getAll();

            $this->set("categorys", $category);
        }

        public function postEdit($auctionId){
            $this->getEdit($auctionId);

            $auctionModel = new \App\Models\AuctionModels($this->getConnection());

            $title              =         filter_input(INPUT_POST, "title", FILTER_SANITIZE_STRING);
            $description        =         filter_input(INPUT_POST, "description", FILTER_SANITIZE_STRING);
            $startingprice      =         sprintf("%.2f",filter_input(INPUT_POST, "startingprice", FILTER_SANITIZE_STRING));
            $startat            =         filter_input(INPUT_POST, "startat", FILTER_SANITIZE_STRING);
            $endsat             =         filter_input(INPUT_POST, "endsat", FILTER_SANITIZE_STRING);
            $categoryid         =         filter_input(INPUT_POST, "categoryid", FILTER_SANITIZE_NUMBER_INT);

            $editData = [
                "title"                 => $title,
                "description"           => $description,
                "starting_price"        => $startingprice,
                "start_at"              => $startat,
                "end_at"                => $endsat,
                "category_id"           => $categoryid
            ];

            $res = $auctionModel->edit($auctionId, $editData);

            if(!$res){
                $this->set("message", "Izn+mena nije uspela");
                return;
            }
            $uploadStatus = $this->doImageUpload("image", $auctionId);

            if(!$uploadStatus){
                //$this->set("message", "Aukcija je izmenjena ali nie dodata nova slika...");
                return;
            }
            $this->notifyAuction($auctionId);
            $this->redirect("/slo/user/auction");
        }

        private function notifyAuction($auctionId){
            $auctionModel = new \App\Models\AuctionModels($this->getConnection());
            $action = $auctionModel->getById($auctionId);

            $html = "<!doctype html><head><meta charset='utf-8'></head><body>";
            $html .= "Dosle je do izmena na vasoj aukcija";
            $html .= $action->title;
            $html .= "</body></html>";

            $mailer = new \PHPMailer\PHPMailer\PHPMailer();
            $mailer->isSMTP();
            $mailer->Host = \Configuration::MAIL_HOST;
            $mailer->Port = \Configuration::MAIL_PORT;
            $mailer->SMTPSecure = \Configuration::MAIL_PROTOCOL;
            $mailer->SMTPAuth = true;
            $mailer->Username = \Configuration::MAIL_USERNAME;
            $mailer->Password = \Configuration::MAIL_PASSWORD;

            $mailer->isHTML(true);
            $mailer->Body = $html;
            $mailer->Subject = "Update Auction";
            $mailer->setFrom( \Configuration::MAIL_USERNAME);

            $mailer->addAddress("slobodan.skaric@gmail.com");

            $mailer->send();
        }
        private function doImageUpload(string $fieldName, string $fileName):bool{
            $uplaodPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            unlink(\Configuration::UPLOAD_DIR .  $fileName .".jpg");
            $file = new \Upload\File($fieldName, $uplaodPath);
            $file->setName($fileName);
            $file->addValidations([
               new \Upload\Validation\Mimetype("image/jpeg"),
               new \Upload\Validation\Size("3M")
            ]);

            try{
                $file->upload();
                return true;
            }catch (\Exception $e){
                $this->set("message", "Greska: " . implode(",", $file->getErrors()));
                return false;
            }
        }
}