<?php

namespace App\Validators;

use App\Core\Validator;

class StirngValidator implements Validator{
    private $minStringLength;
    private $maxStringLength;

    public function __construct(){
        $this->minStringLength = 1;
        $this->maxStringLength = 255;        
    }

    public function setMinLength(int $length): StirngValidator{
        $this->minStringLength = max(0, $length);
        return $this;
    }

    public function setMaxlength(int $length): StirngValidator{
        $this->maxStringLength = max(1 , $length);
        return $this;
    }
    public function isValid(string $value):bool{
       $len = strlen($value);

       return boolval($this->minStringLength <= $len && $len <= $this->maxStringLength);
    }
}