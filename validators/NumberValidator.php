<?php

namespace App\Validators;
use App\Core\Validator;

class NumberValidator implements Validator{
    private $isSingt;
    private $integerLength;
    private $isReal;
    private $maxDecimalDigit;

    public function __construct(){
        $this->isSingt = false;
        $this->isReal = false;
        $this->integerLength = 10;
        $this->maxDecimalDigit = 0;
    }

    public function setInteger(): NumberValidator{
        $this->isReal = false;
        return $this;
    }

    public function setDecimal(): NumberValidator{
        $this->isReal = true;
        return $this;
    }

    public function setSigned(): NumberValidator{
        $this->isSingt = true;
        return $this;
    }

    public function setUnsigned(): NumberValidator{
        $this->isSingt = false;
        return $this;
    }

    public function setIntegerlength(int $length): NumberValidator{
        $this->integerLength = max(1, $length);
        return $this;
    }

    public function setMaxDecimalDigits(int $maxDigits): NumberValidator{
        $this->maxDecimalDigit = max(0, $maxDigits);
        return $this;
    }


    public function isValid(string $value):bool{
        $pattern = "/^";

        if($this->isSingt === true){
            $pattern .= "\-?";
        }

        $pattern .= "[1-9][0-9]{0," . ($this->integerLength-1) ."}";

        if($this->isReal === true){
            $pattern .= "\.[0-9]{0," . $this->maxDecimalDigit . "}";
        }

        $pattern .= "$/";

        return boolval(preg_match($pattern, $value));
    }
}