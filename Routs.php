<?php

    return [

        App\Core\Route::get("|^user/register/?$|",                         "Main",        "getRegister"),
        App\Core\Route::post("|^user/register/?$|",                        "Main",        "postRegister"),
        App\Core\Route::get("|^user/login/?$|",                            "Main",        "getLogin"),
        App\Core\Route::post("|^user/login/?$|",                           "Main",        "postLogin"),
        App\Core\Route::get("|^user/logout$/?|",                           "Main",        "getLogout"),

        #redirect
        #roll
        App\Core\Route::get("|^user/profile/?$|",                          "UserDashbord",        "index"),
        App\Core\Route::get("|^user/categories/?$|",                       "UserCategoryMenagment", "categorys"),
        App\Core\Route::get("|^user/categories/edit/([0-9]+)/?$|",         "UserCategoryMenagment", "getEdit"),
        App\Core\Route::post("|^user/categories/edit/([0-9]+)?$|",         "UserCategoryMenagment", "postEdit"),
        App\Core\Route::get("|^user/categories/add/?$|",                   "UserCategoryMenagment", "getAdd"),
        App\Core\Route::post("|^user/categories/add/?$|",                  "UserCategoryMenagment", "postAdd"),
        App\Core\Route::post("|^user/search/?$|",                          "Auction", "postSearch"),

        App\Core\Route::get("|^user/auction/?$|",                       "UserAuctionMenagment", "auctions"),
        App\Core\Route::get("|^user/auction/edit/([0-9]+)/?$|",         "UserAuctionMenagment", "getEdit"),
        App\Core\Route::post("|^user/auction/edit/([0-9]+)?$|",         "UserAuctionMenagment", "postEdit"),
        App\Core\Route::get("|^user/auction/add/?$|",                   "UserAuctionMenagment", "getAdd"),
        App\Core\Route::post("|^user/auction/add/?$|",                  "UserAuctionMenagment", "postAdd"),

        App\Core\Route::get("|^category/([0-9]+)/?$|",                     "Category",     "show"),
        App\Core\Route::get("|^auction/([0-9]+)/?$|",                      "Auction",      "show"),
        
        #Api rout
        App\Core\Route::get("|^api/auction/([0-9]+)/?$|",                  "ApiAuction",   "show"),
        App\Core\Route::get("|^api/bookmarks/?$|",                         "ApiBookmarks", "getBookmarks"),
        App\Core\Route::get("|^api/bookmarks/add/([0-9]+)$|",              "ApiBookmarks", "addBookmarks"),
        App\Core\Route::get("|^api/bookmarks/clear/?$|",                   "ApiBookmarks", "clear" ),

        #FoldBackRoot
        App\Core\Route::any("|^.*$|", "Main", "home")

    ];