<?php
  require_once "Configuration.php";
  require_once "vendor/autoload.php";

  ob_start();

  use App\Core\DatabseConfiguration;
  use App\Core\DatabaseConnection;

  $configuration = new DatabseConfiguration(
    Configuration::DATABASE_HOST,
    Configuration::DATABASE_DB,
    Configuration::DATABASE_USER,
    Configuration::DATABASE_PASS
    );
  $connection = new DatabaseConnection($configuration);

  $url = filter_input(INPUT_GET, "URL");
  $httpMethod = filter_input(INPUT_SERVER, "REQUEST_METHOD");

  $router = new App\Core\Router();
  $routs = require_once "Routs.php";

  foreach($routs as $route){
    $router->add($route);
  }

  $rout = $router->find($httpMethod, $url);
  $arguments = $rout->extractAgruments($url);
 
  $controllerName = "\\App\\Controllers\\" . $rout->getControllerName() . "Controller";
  $controller = new $controllerName($connection);

  $fingerPrintProviderFactoryClass = Configuration::FINGERPRINT_PROVIDED_FACTORY;
  $fingerPrintProviderFactoryMethod = Configuration::FINGERPRINT_PROVIDED_METHOD;
  $fingerPrintProviderFactoryArgs = Configuration::FINGERPRINT_PROVIDED_ARGS;

  $fingerPrintProviderFactory = new $fingerPrintProviderFactoryClass;
  $fingerPrintProvider = $fingerPrintProviderFactory->$fingerPrintProviderFactoryMethod(...$fingerPrintProviderFactoryArgs);

  $sessionStorageCalssName = Configuration::SESSION_STORAGE;
  $seesionStorageConstructorArguments = Configuration::SESSION_STORAGE_DATA;
  $sessionStorage = new $sessionStorageCalssName(...$seesionStorageConstructorArguments);

  $session = new App\Core\Session\Session($sessionStorage, Configuration::SESSION_LIFETIME);
  $session->setfingerprintProvider($fingerPrintProvider);

  $controller->setSession($session);
  $controller->getSession()->reload();
  $controller->__pre();
  call_user_func_array([$controller, $rout->getMethodName()],$arguments);
  $controller->getSession()->save();
  
  $data = $controller->getData();
 /* foreach($data as $name => $value){
    $$name = $value;
  }

  require_once "views/" . $rout->getControllerName(). "/" . $rout->getMethodName() . ".php";*/

 /* echo $rout->getControllerName() . '/' . $rout->getMethodName() . '.html';
  print_r($data);
  exit;*/

  if($controller instanceof \App\Core\ApiController){
    ob_clean();
    header("Content-type: application/json; charset=utf-8");
    header("Access-Control-Allow-Origin: *");
    echo json_encode($data);
    exit;
  }


  $loader = new Twig_Loader_Filesystem("./views");
  $twig = new Twig_Environment($loader, [
    "cache" => "./twig_cache",
    "auto_reload" => true
  ]);

echo $twig->render($rout->getControllerName() . '/' . $rout->getMethodName() . '.html', $data);