<?php

    final class Configuration{
        const DATABASE_HOST = "localhost";
        const DATABASE_USER = "root";
        const DATABASE_PASS = "";
        const DATABASE_DB = "users";

        const SESSION_STORAGE = "\\App\\Core\\Session\\FileSessionStorage";
        const SESSION_STORAGE_DATA = ['./session/'];
        const SESSION_LIFETIME = 3600;


        const FINGERPRINT_PROVIDED_FACTORY = "\\App\\Core\\Fingerprint\\BasicFingerprintProviderFactory";
        const FINGERPRINT_PROVIDED_METHOD = "getInstance";
        const FINGERPRINT_PROVIDED_ARGS = ["SERVER"];

        const UPLOAD_DIR = "assets/uploads/";

        const MAIL_HOST = "smtp.gmail.com";
        const MAIL_PORT = "587";
        const MAIL_PROTOCOL = "tls";
        const MAIL_USERNAME = "prosourcesl@gmail.com";
        const MAIL_PASSWORD = "####";
    }