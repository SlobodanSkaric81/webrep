function getBookmarks(){
    fetch("/slo/api/bookmarks", {credentials : "include"})
        .then(result => result.json())
        .then(data => {
            displayBookamrks(data.bookmarks);
        });

}

function addBookmark(auctionId){
    fetch("/slo/api/bookmarks/add/" + auctionId, {credentials : "include"})
        .then(result => result.json())
        .then(data => {
            if(data.error === 0){
                getBookmarks();
            }
        });
}

function celarBookmark(){
    fetch("/slo/api/bookmarks/clear" , {credentials : "include"})
        .then(result => result.json())
        .then(data => {
            if(data.error === 0){
                getBookmarks();
            }
        });
}

function displayBookamrks(bookmarks){
    const bookmarkDiv = document.querySelector(".bookmarks");
    bookmarkDiv.innerHTML = "";

    if(bookmarks.length == 0){
        bookmarkDiv.innerHTML = "No bookmarks...";
        return;
    }

    for(bookmark of bookmarks){
        const linkToArt = document.createElement("a");
        linkToArt.style.display = "block";
        linkToArt.innerHTML = bookmark.title;
        linkToArt.href = "/slo/auction/" + bookmark.auction_id;

        bookmarkDiv.appendChild(linkToArt);
    }
}

addEventListener("load", getBookmarks);